import 'package:flutter/material.dart';

/// Class containing constants used throughout the app
class Constants {
  static const appTitle = "Flutter Test Index";
  static const loginTitle = "Login";
  static const homeTitle = "Home";
  static const settingsTitle = "Settings";
  static const fullPhotoTitle = "Full Photo";
}

class AppColors {
  static const themeColor = Color(0xfff5a623);
  static const primaryColor = Color.fromRGBO(80, 149, 148, 1);
  static const secondaryColor = Color.fromRGBO(72, 128, 200, 1);

  static const primaryLight = Color.fromRGBO(236, 249, 252, 1);
  static const primaryAqua = Color.fromRGBO(154, 227, 217, 1);

  static const grey = Color.fromRGBO(77, 84, 103, 1);
  static const greyLighter = Color.fromRGBO(116, 128, 148, 1);

  static const secondaryLight = Color.fromRGBO(237, 247, 253, 1);
  static const secondaryLighter = Color.fromRGBO(235, 254, 251, 1);
}

class ImageConstants {
  static const String splash = "assets/splash.png";
  static const String handshake = "assets/handshake.png";
  static const String first = "assets/first.png";
  static const String second = "assets/second.png";
  static const String third = "assets/third.png";
  static const String fourth = "assets/fourth.png";
  static const String fifth = "assets/fifth.png";
}
