// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const DESKTOP = _Paths.DESKTOP;
  static const MOBILE = _Paths.MOBILE;
}

abstract class _Paths {
  static const DESKTOP = '/desktop';
  static const MOBILE = '/mobile';
}
