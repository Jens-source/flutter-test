import 'package:flutter/foundation.dart';
import 'package:fluttertestindex/core/bindings/on_boarding_bindings.dart';
import 'package:fluttertestindex/presentation/pages/desktop/desktop_screen.dart';
import 'package:fluttertestindex/presentation/pages/mobile/mobile_screen.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
part 'app_routes.dart';

class AppPages {
  AppPages._();

  static String initial = kIsWeb ? Routes.DESKTOP : Routes.MOBILE;

  static final routes = [
    GetPage(
        name: _Paths.MOBILE,
        page: () => const MobileScreen(),
        binding: OnBoardingBindings()),
    GetPage(
        name: _Paths.DESKTOP,
        page: () => const DesktopScreen(),
        binding: OnBoardingBindings()),
  ];
}
