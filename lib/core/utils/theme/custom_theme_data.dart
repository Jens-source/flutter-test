import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomTextTheme extends TextTheme {
  ///
  @override
  TextStyle? get labelLarge =>
      GoogleFonts.openSans(fontWeight: FontWeight.w600, fontSize: 16);
}

class Themes {
  static final light = ThemeData.light().copyWith(
    cardColor: const Color(0xff17494d),
    textTheme: CustomTextTheme(),
    backgroundColor: const Color(0xfffdf8ee),
    canvasColor: const Color(0xfffdf8ee),
  );

  static final dark = ThemeData.dark().copyWith(
    cardColor: const Color(0xff17494d),
    textTheme: CustomTextTheme(),
    backgroundColor: const Color(0xfffdf8ee),
    canvasColor: const Color(0xfffdf8ee),
  );
}
