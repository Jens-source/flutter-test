import 'package:flutter/material.dart';
import 'package:fluttertestindex/core/config/app_constants.dart';

class CurvedPainter1 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = AppColors.primaryLight
      ..style = PaintingStyle.fill
      ..strokeWidth = 15;
    var paint2 = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill
      ..strokeWidth = 15;

    Path path = Path();
    path.moveTo(0, 0);
    path.lineTo(0, size.height * 0.8);
    path.lineTo(size.width / 2, size.height / 2);
    path.close();

    canvas.drawPath(path, paint);
    path.moveTo(0, size.height * 0.8);

    path.quadraticBezierTo(
        size.width / 4, size.height + 20, size.width / 2, size.height / 2);
    canvas.drawPath(path, paint);

    Path path2 = Path();
    path2.moveTo(size.width / 2, size.height / 2);
    path2.quadraticBezierTo(
        size.width / 4 * 3, -20, size.width, size.height * 0.2);

    canvas.drawPath(path2, paint2);

    path2.moveTo(size.width, size.height);
    path2.lineTo(size.width, size.height * 0.2);
    path2.lineTo(size.width / 2, size.height / 2);
    path2.close();
    canvas.drawPath(path2, paint2);

    //
    // path2.moveTo(size.width / 2, size.height);
    //
    // path2.quadraticBezierTo(size.width / 4,
    //     transpose ? size.height + 100 : size.height - 100, 0, size.height);
    //
    // canvas.drawPath(path2, paint2);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

class CurvedPainter2 extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = AppColors.primaryLight
      ..style = PaintingStyle.fill
      ..strokeWidth = 15;
    var paint2 = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill
      ..strokeWidth = 15;

    Path path = Path();
    path.moveTo(0, size.height * 0.13);
    path.quadraticBezierTo(
        size.width * 0.1, -25, size.width / 4, size.height / 8);
    canvas.drawPath(path, paint);
    Path path2 = Path();
    path2.moveTo(size.width / 4, size.height / 8);

    path2.quadraticBezierTo(
        size.width / 2, size.height / 4, size.width / 4 * 3, size.height / 8);
    canvas.drawPath(path2, paint2);
    path.moveTo(size.width / 4 * 3, size.height / 8);
    path.quadraticBezierTo(size.width * 0.95, -50, size.width, size.height / 3);
    canvas.drawPath(path, paint);

    path.moveTo(0, size.height - (size.height * 0.13));
    path.quadraticBezierTo(size.width * 0.1, size.height + 25, size.width / 4,
        size.height - (size.height / 8));
    canvas.drawPath(path, paint);
    path2.moveTo(size.width / 4, size.height - (size.height / 8));
    path2.quadraticBezierTo(size.width / 2, size.height - (size.height / 4),
        size.width / 4 * 3, size.height - (size.height / 8));
    canvas.drawPath(path2, paint2);
    path.moveTo(size.width / 4 * 3, size.height - (size.height / 8));
    path.quadraticBezierTo(size.width * 0.95, size.height + 50, size.width,
        size.height - (size.height / 3));
    canvas.drawPath(path, paint);

    path.moveTo(size.width / 4 * 3, size.height - (size.height / 8));
    path.lineTo(size.width / 4 * 3, size.height / 8);
    path.lineTo(size.width, size.height / 3);
    path.lineTo(size.width, size.height - (size.height / 3));

    path.close();
    canvas.drawPath(path, paint);

    // Path path2 = Path();
    // path2.moveTo(size.width / 2, size.height / 2);
    // path2.quadraticBezierTo(
    //     size.width / 4 * 3, -20, size.width, size.height * 0.2);
    //
    // canvas.drawPath(path2, paint2);
    //
    // path2.moveTo(size.width, size.height);
    // path2.lineTo(size.width, size.height * 0.2);
    // path2.lineTo(size.width / 2, size.height / 2);
    // path2.close();
    // canvas.drawPath(path2, paint2);

    //
    // path2.moveTo(size.width / 2, size.height);
    //
    // path2.quadraticBezierTo(size.width / 4,
    //     transpose ? size.height + 100 : size.height - 100, 0, size.height);
    //
    // canvas.drawPath(path2, paint2);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
