import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertestindex/core/common_widgets/curved_painter.dart';
import 'package:get/get.dart';

import '../config/app_constants.dart';

class CommonTaskCard extends StatelessWidget {
  const CommonTaskCard({
    Key? key,
    required this.topAligned,
    required this.image,
    required this.number,
    required this.text,
    required this.paint,
  }) : super(key: key);

  final bool topAligned;
  final int number;
  final String text;
  final String image;
  final bool paint;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 320,
      width: Get.width,
      child: Stack(
        alignment: Alignment.center,
        children: [
          paint
              ? Positioned(
                  left: 0,
                  child: Container(
                    color: AppColors.primaryLight,
                    height: 240,
                    width: kIsWeb ? Get.width / 4 * 3 : 700 / 4 * 3,
                  ),
                )
              : Container(),
          paint
              ? Positioned(
                  height: 320,
                  width: kIsWeb ? Get.width : 700,
                  child: CustomPaint(
                    painter: CurvedPainter2(),
                  ),
                )
              : Container(),
          Positioned(
            top: kIsWeb
                ? 0 : topAligned ? 0 : null,
            bottom: kIsWeb ? 0 : topAligned ? null : 0,
            right: kIsWeb ? topAligned ? Get.width / 4 : null: null,
            left: kIsWeb ? topAligned ? null : Get.width / 4: null,
            child: Container(
                padding: const EdgeInsets.only(top: 40, right: 30, bottom: 20),
                height: 220,
                child: Image.asset(image)),
          ),
          Positioned(
            bottom: kIsWeb? 0 : topAligned ? 0 : null,
            top: kIsWeb ? 0 : topAligned ? null : 0,
            right: kIsWeb ? topAligned ? null : Get.width / 4 : null,
            left: kIsWeb ? topAligned ? Get.width / 4 : null: null,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: topAligned
                          ? Colors.transparent
                          : Colors.black.withAlpha(5)),
                  child: Text(
                    "$number.",
                    style: Get.textTheme.labelLarge!.apply(
                        fontSizeDelta: 100, color: AppColors.greyLighter),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 30, left: 10),
                  child: Text(
                    text,
                    style: Get.textTheme.labelLarge!
                        .apply(color: AppColors.greyLighter),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
