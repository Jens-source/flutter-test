import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:get/get.dart';

class OnBoardingController extends GetxController {
  final ScrollController _scrollController = ScrollController();

  ScrollController get scrollController => _scrollController;

  final RxInt _aboutIndex = 0.obs;
  final RxInt _aboutHover = 0.obs;

  int get aboutIndex => _aboutIndex.value;
  int get aboutHover => _aboutHover.value;

  void setAboutHover(bool hover, int index) {
    if (hover) {
      _aboutHover.value = index;
    } else {
      _aboutHover.value = 4;
    }
    _aboutHover.refresh();
  }

  void setAboutIndex(int index) {
    if (index == 0) {
      _scrollController.animateTo(0,
          duration: const Duration(milliseconds: 500),
          curve: Curves.decelerate);
    } else if (index == 1) {
      _scrollController.animateTo(100,
          duration: const Duration(milliseconds: 500),
          curve: Curves.decelerate);
    } else {
      _scrollController.animateTo(200,
          duration: const Duration(milliseconds: 500),
          curve: Curves.decelerate);
    }
    _aboutIndex.value = index;
    _aboutIndex.refresh();
  }

  Future<void> initSplash() async {
    await Future.delayed(const Duration(seconds: 2));
    FlutterNativeSplash.remove();
  }

  @override
  void onInit() {
    initSplash();
    super.onInit();
  }
}
