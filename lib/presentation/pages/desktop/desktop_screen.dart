import 'package:flutter/material.dart';
import 'package:fluttertestindex/core/common_widgets/curved_painter.dart';
import 'package:fluttertestindex/core/config/app_constants.dart';
import 'package:fluttertestindex/core/controllers/on_boarding_controller.dart';
import 'package:fluttertestindex/presentation/pages/desktop/widgets/about.dart';
import 'package:fluttertestindex/presentation/pages/desktop/widgets/header.dart';
import 'package:fluttertestindex/presentation/pages/mobile/widgets/about.dart';
import 'package:fluttertestindex/presentation/pages/mobile/widgets/header.dart';
import 'package:get/get.dart';

class DesktopScreen extends GetView<OnBoardingController> {
  const DesktopScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
          ),
        ),
        backgroundColor: Colors.white,
        actions: [
          TextButton(
              onPressed: () {},
              child: const Text(
                "Login",
                style: TextStyle(color: AppColors.primaryColor),
              ))
        ],
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [DesktopHeader(), DesktopAbout()],
            ),
          ),
        ],
      ),
    );
  }
}
