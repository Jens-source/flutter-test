import 'package:flutter/material.dart';
import 'package:fluttertestindex/core/common_widgets/curved_painter.dart';
import 'package:fluttertestindex/core/config/app_constants.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import 'about.dart';

class DesktopHeader extends StatelessWidget {
  const DesktopHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            width: Get.width,
            height: Get.height / 2 + 100,
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 20),
                  height: Get.height / 2,
                  width: Get.width,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight,
                      colors: [
                        AppColors.secondaryLight,
                        AppColors.secondaryLighter
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 100,
                  child: Container(
                    width: 250,
                    height: 250,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage(ImageConstants.handshake))),
                  ),
                ),
                Positioned(
                    top: Get.height / 2 - 50,
                    child: SizedBox(
                      height: 100,
                      width: Get.width,
                      child: CustomPaint(
                        painter: CurvedPainter1(),
                      ),
                    )),
                Positioned(
                  left: Get.width / 8,
                  top: Get.height / 7,
                  child: Container(
                    padding: const EdgeInsets.only(top: 20),
                    child: Column(
                      children: [
                        Text(
                          "Deine Job\nwebsite",
                          style: Get.textTheme.labelLarge!
                              .apply(color: Colors.black, fontSizeDelta: 15),
                          textAlign: TextAlign.center,
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            margin: const EdgeInsets.only(top: 30),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 40, vertical: 5),
                            decoration: BoxDecoration(
                              gradient: const LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  AppColors.primaryColor,
                                  AppColors.secondaryColor
                                ],
                              ),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Center(
                              child: Text("Kostenlos Registrieren",
                                  style: Get.textTheme.labelLarge!.apply(
                                      color: Colors.white, fontSizeDelta: -5)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
