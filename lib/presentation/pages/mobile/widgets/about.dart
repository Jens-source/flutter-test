import 'package:flutter/material.dart';
import 'package:fluttertestindex/core/common_widgets/common_tasks_card.dart';
import 'package:fluttertestindex/core/config/app_constants.dart';
import 'package:fluttertestindex/core/controllers/on_boarding_controller.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../../../core/common_widgets/curved_painter.dart';

class MobileAbout extends StatelessWidget {
  MobileAbout({Key? key}) : super(key: key);

  final List<String> headers = [
    "Drei einfache Schitte zu deinem neuen Job",
    "Drei einfache Schitte zu deinem neuen Mitarbeiter",
    "Drei einfache Schitte zu Vermittlung neuer Mitarbeiter"
  ];

  @override
  Widget build(BuildContext context) {
    return GetX<OnBoardingController>(
      builder: (builder) => SizedBox(
        width: Get.width,
        child: Column(
          children: [
            SingleChildScrollView(
              controller: builder.scrollController,
              physics: const BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 30),
                height: builder.aboutIndex == 0 ? 60 : 60,
                width: 450,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  border: Border.all(color: Colors.black12, width: 2),
                ),
                child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: 3,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (_, index) {
                      String text = "";

                      if (index == 0) {
                        text = "Arbeitnehmer";
                      } else if (index == 1) {
                        text = "Arbeitgebher";
                      } else {
                        text = "Temporarburo";
                      }

                      return Row(
                        children: [
                          InkWell(
                            onHover: (hover) =>
                                builder.setAboutHover(hover, index),
                            onTap: () => builder.setAboutIndex(index),
                            child: AnimatedContainer(
                              curve: Curves.decelerate,
                              duration: const Duration(milliseconds: 500),
                              height: 59,
                              width: 149,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft:
                                        Radius.circular(index == 0 ? 20 : 0),
                                    bottomLeft:
                                        Radius.circular(index == 0 ? 20 : 0),
                                    topRight:
                                        Radius.circular(index == 2 ? 20 : 0),
                                    bottomRight:
                                        Radius.circular(index == 2 ? 20 : 0)),
                                color: index == builder.aboutIndex
                                    ? AppColors.primaryAqua
                                    : builder.aboutHover == index
                                        ? AppColors.secondaryLighter
                                        : Colors.white,
                              ),
                              child: Center(
                                child: Text(
                                  text,
                                  style: Get.textTheme.labelLarge!.apply(
                                      color: builder.aboutIndex == index
                                          ? Colors.white
                                          : AppColors.primaryAqua),
                                ),
                              ),
                            ),
                          ),
                          index == 0 || index == 1
                              ? Container(
                                  height: 59,
                                  color: Colors.black12,
                                  width: 1,
                                )
                              : Container()
                        ],
                      );
                    }),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 30),
              child: Text(
                headers[builder.aboutIndex],
                style: Get.textTheme.labelLarge!
                    .apply(color: AppColors.grey, fontSizeDelta: 12),
                textAlign: TextAlign.center,
              ),
            ),
            const CommonTaskCard(
              topAligned: false,
              text: "Erstellen dein Lebenslauf",
              image: ImageConstants.first,
              number: 1,
              paint: false,
            ),
            const CommonTaskCard(
              topAligned: true,
              text: "Erhalte Vermittlungs-\nangebot von Arbeitgeber",
              image: ImageConstants.fourth,
              number: 2,
              paint: true,
            ),
            const CommonTaskCard(
              topAligned: false,
              text: "Vermittlungs nach\nProvision oder\nStundenlohn",
              image: ImageConstants.fifth,
              number: 3,
              paint: false,
            ),
            100.heightBox,
          ],
        ),
      ),
    );
  }
}
