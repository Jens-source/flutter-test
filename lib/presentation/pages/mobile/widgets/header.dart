import 'package:flutter/material.dart';
import 'package:fluttertestindex/core/common_widgets/curved_painter.dart';
import 'package:fluttertestindex/core/config/app_constants.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import 'about.dart';

class MobileHeader extends StatelessWidget {
  const MobileHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            width: Get.width,
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                Container(
                  padding: const EdgeInsets.only(top: 20),
                  height: Get.height,
                  width: Get.width,
                  color: AppColors.primaryLight,
                ),
                Positioned(
                  top: 100,
                  left: -120,
                  right: -100,
                  child: SizedBox(
                      width: Get.width + 100,
                      child: Image.asset(
                        ImageConstants.handshake,
                        fit: BoxFit.fitWidth,
                      )),
                ),
                Positioned(
                    top: Get.height - 50,
                    left: 0,
                    child: SizedBox(
                      height: 100,
                      width: Get.width,
                      child: CustomPaint(
                        painter: CurvedPainter1(),
                      ),
                    )),
                Container(
                  padding: const EdgeInsets.only(top: 20),
                  child: Column(
                    children: [
                      Text(
                        "Deine Job\nwebsite",
                        style: Get.textTheme.labelLarge!
                            .apply(color: Colors.black, fontSizeDelta: 20),
                        textAlign: TextAlign.center,
                      ),
                      700.heightBox,
                      MobileAbout(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
