import 'package:flutter/material.dart';
import 'package:fluttertestindex/core/config/app_constants.dart';
import 'package:fluttertestindex/core/controllers/on_boarding_controller.dart';
import 'package:fluttertestindex/presentation/pages/mobile/widgets/header.dart';
import 'package:get/get.dart';

class MobileScreen extends GetView<OnBoardingController> {
  const MobileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
          ),
        ),
        backgroundColor: Colors.white,
        actions: [
          TextButton(
              onPressed: () {},
              child: const Text(
                "Login",
                style: TextStyle(color: AppColors.primaryColor),
              ))
        ],
      ),
      bottomSheet: Container(
        height: 80,
        width: Get.width,
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(20), topLeft: Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0, -3),
                  spreadRadius: 3,
                  blurRadius: 2)
            ]),
        child: Center(
          child: GestureDetector(
            onTap: () {},
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              decoration: BoxDecoration(
                gradient: const LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  colors: [AppColors.primaryColor, AppColors.secondaryColor],
                ),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Center(
                child: Text("Kostenlos Registrieren",
                    style: Get.textTheme.labelLarge),
              ),
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: const [MobileHeader()],
        ),
      ),
    );
  }
}
